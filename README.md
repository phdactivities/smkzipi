# smkZipi

## Features available by version.

| Features                                      | v0.0.1 |
| :---                                          | :---:  |
| Project structure                             | OK     |
| Generation of Logs                            | OK     |
| Configuration file.                           | OK     |
| Selection of environment variables.           | OK     |
| Pipeline for image build.                     | OK     |
| Access to execute Docker commands.            | OK     |
| Test for TCP connections                      | OK     |
| Print summary table.                          | OK     |



[![Quality Gate Status](http://localhost:9000/api/project_badges/measure?project=zipi-smokeTest&metric=alert_status)](http://localhost:9000/dashboard?id=zipi-smokeTest)

