const scanner = require('sonarqube-scanner');

scanner(
    {
        serverUrl: 'http://localhost:9000',
        token: '2fc5d50221be33ad2111d9d63a0f0db93af0862f',
        options: {
            'sonar.projectName': 'zipi-smokeTest',
            'sonar.projectDescription': "Zipi. tecnology for create smoke test in micro-services",
            'sonar.sources': './',
            'sonar.test': 'test',
            'sonar.javascript.coveragePlugin': 'lcov',
            'sonar.javascript.lcov.reportPaths': 'output/coverage/jest/lcov.info'
            }
        },
        () => process.exit()
)